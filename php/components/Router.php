<?php

class Router
{
    private string $routesPath = ROOT.'/php/config/routes.php';
    private string $controllerPath = ROOT.'/php/controllers/';

    private array $routes;

    /**
     * Router constructor.
     * @noinspection PhpIncludeInspection
     */
    public function __construct() {
        $this->routes = require $this->routesPath;
    }

    private function getURI() {
        return trim($_SERVER['REQUEST_URI'], '/');
    }


     /* @noinspection PhpUnhandledExceptionInspection
     * @param string $controllerName (название контроллера)
     * @return mixed (экземпляр контроллера)
     * @throws ActionNotFoundException
     */
    private function getController(string $controllerName) {
        $controllerFile = $this->controllerPath . $controllerName . '.php';

        if (!file_exists($controllerFile)) {
            throw new ActionNotFoundException("Файл $controllerFile не найден");
        }

        if (!class_exists($controllerName)) {
            throw new ActionNotFoundException("Класс $controllerName в файле $controllerFile не найден");
        }

        return new $controllerName();
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    private function callAction(string $controllerName,
                                string $actionName,
                                array $params) {

        $controller = $this->getController($controllerName);

        if(!method_exists($controller, $actionName)) {
            throw new ActionNotFoundException("Метод $actionName в классе $controllerName");
        }

        call_user_func_array([$controller, $actionName], $params);
    }

    public function run() {
        $uri = $this->getURI();

        foreach ($this->routes as $uriPattern => $path) {

            if (!preg_match("~$uriPattern~", $uri)) {
                continue;
            }

            $internalRoute = preg_replace("~$uriPattern~", $path, $uri);

            $parts = explode('/', $internalRoute);

            $controllerName = ucfirst(array_shift($parts)).'Controller';
            $actionName = 'action'.ucfirst(array_shift($parts));
            $params = $parts;

            try {
                $this->callAction($controllerName, $actionName, $params);
                break;
            }
            catch (ActionNotFoundException $e) {
            }
        }
    }
}