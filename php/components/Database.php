<?php


class Database
{
    private static string $settingsPath = ROOT.'/php/config/dbSettings.php';


    /** @noinspection PhpIncludeInspection */
    public static function getConnection(): PDO {
        $settings = require self::$settingsPath;
        $dsn = "{$settings['managementSystem']}:host={$settings['host']};dbname={$settings['name']}";
        return new PDO($dsn, $settings['user'], $settings['password']);
    }
}