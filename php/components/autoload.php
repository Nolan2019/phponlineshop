<?php


/** @noinspection PhpIncludeInspection */
spl_autoload_register(function ($class_name) {
    $array_paths = ['/php/controllers/', '/php/models/', '/php/components/', '/php/exceptions/'];

    foreach ($array_paths as $path) {
        $full_path = ROOT.$path.$class_name.'.php';

        if (is_file($full_path)) {
            include $full_path;
        }
    }
});