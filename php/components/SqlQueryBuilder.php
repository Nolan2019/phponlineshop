<?php


class SqlQueryBuilder
{
    // $limit и $skip работают только для MySQL, H2, HSQLDB, Postgres и SQLite
    public static function getSql(
        array $tables,
        array $fields=['*'],
        array $join=null,
        array $filters=null,
        string $orderByField=null,
        bool $ascending=true,
        int $limit=null,
        int $skip=null
    ): string {
        $fields = implode(', ', $fields);
        $table = array_shift($tables);

        $result = "SELECT $fields FROM $table";

        // генерация соединений с таблицами
        if (isset($join) and count($tables) == count($join)) {
            $i = 0;
            if (is_string(array_keys($join)[0])) {
                foreach ($join as $firstField=> $secondField) {
                    $result .= " JOIN $tables[$i] ON $table.$firstField = $tables[$i].$secondField";
                    $i++;
                }
            }
            else {
                foreach ($join as $field) {
                    $result .= " JOIN $tables[$i] USING ($field)";
                    $i++;
                }
            }
        }
        else if (count($tables) > 0) {
            foreach ($tables as $table) {
                $result .= " NATURAL JOIN $table";
            }
        }

        // генерация условий WHERE
        if (isset($filters)) {
            $filters = array_map('strval', $filters);
            $result .= " WHERE ".implode(' AND ', $filters);
        }

        if (isset($orderByField)) {
            $sortMethod = $ascending ? 'ASC' : 'DESC';
            $result .= " ORDER BY $orderByField $sortMethod";
        }

        if (isset($limit)) {
            $result .= " LIMIT $limit";
        }

        if (isset($skip)) {
            $result .= " OFFSET $skip";
        }

        return $result;
    }
}