<?php

/*
 * Класс для генерации постраничной навигации
 */

class Pagination {
    private int $linkLimit = 5; // максимальное количество ссылок на другие страницы
    private string $index = 'page';
    private int $currentPageNumber; // номер текущей страницы
    private int $itemTotalCount; // общее количество элементов, отображаемых на страницах
    private int $itemLimit; // максимальное количество элементов на странице

    private int $pageCount;

    public function __construct(int $currentPageNumber, int $itemTotalCount, int $itemLimit, string $index)
    {
        $this->itemTotalCount = $itemTotalCount;
        $this->itemLimit = $itemLimit > 0 ? $itemLimit : 1;
        $this->index = $index;
        $this->pageCount = $this->pageCount();
        $this->setCurrentPage($currentPageNumber);
    }

    public function __toString() {

        $links = null; // строка ссылок

        $limits = $this->limits(); // ограничения цикла

        for ($page = $limits[0]; $page <= $limits[1]; $page++) { // генерация ссылок
            if ($page == $this->currentPageNumber) {
                # генерируем нерабочую ссылку с классом active
                $links .= '<li class="active"><a href="#">'.$page.'</a></li>';
            } else {
                $links .= $this->generateHtml($page); // генерируем рабочую ссылку
            }
        }

        if (isset($links)) {
            if ($this->currentPageNumber > 1)
                # добавляем ссылку на первую страницу
                $links = $this->generateHtml(1, 'первая') . $links;

            # если это не полследняя страница
            if ($this->currentPageNumber < $this->pageCount)
                # добавляем ссылку на последнюю страницу
                $links .= $this->generateHtml($this->pageCount, 'последняя');
        }

        # Возвращаем html
        return "<ul class='pagination'>".$links .'</ul>';
    }

    // Для генерации HTML-кода ссылки
    private function generateHtml($page, $text = null)
    {
        if (!$text)
            $text = $page;

        //$currentURI = rtrim(dirname($_SERVER['REQUEST_URI']), '\\');
        $currentURI = rtrim($_SERVER['REQUEST_URI'], '/') . '/';
        $currentURI = preg_replace('~/page[1-9][0-9]*~', '', $currentURI);
        return
            '<li><a href="'.$currentURI.$this->index.$page.'">'.$text.'</a></li>';
    }

    private function limits()
    {
        # Вычисляем ссылки слева (чтобы активная ссылка была посередине)
        $start = $this->currentPageNumber - round($this->linkLimit / 2) + 1;
        $start = $start > 0 ? $start : 1;

        $end = $start + $this->linkLimit - 1;

        if ($end > $this->pageCount) {
            $gap = $end - $this->pageCount;
            $end = $this->pageCount;

            $start = $start > $gap ? $start - $gap : 1;
        }

        return [$start, $end];
    }


    // Для установки номера текущей страницы (с валидацией и автокорректировкой)
    private function setCurrentPage(int $currentPageNumber)
    {
        $this->currentPageNumber = $currentPageNumber;

        if ($this->currentPageNumber > 0) {
            if ($this->currentPageNumber > $this->pageCount)
                $this->currentPageNumber = $this->pageCount;
        } else
            $this->currentPageNumber = 1;
    }

    private function pageCount(): int
    {
        return round($this->itemTotalCount / $this->itemLimit);
    }

}
