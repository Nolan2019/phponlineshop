<?php


class fileFinder
{
    public static function getFiles(string $directory): string {
        if (!is_dir($directory))
            return '[]';
        $files = scandir($directory);
        array_shift($files);
        array_shift($files);

        return count($files) > 0 ? "['" . implode("', '", array_values($files)) . "']" : '[]';
    }
}