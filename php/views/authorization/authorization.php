<html class=" responsive" lang="ru">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<title>Войти</title>

		<link rel="shortcut icon" href="/styles/authorization/favicon.ico" type="image/x-icon">
		<link href="/styles/authorization/motiva_sans.css" rel="stylesheet" type="text/css">
		<link href="/styles/authorization/shared_global.css" rel="stylesheet" type="text/css">
		<link href="/styles/authorization/buttons.css" rel="stylesheet" type="text/css">
		<link href="/styles/authorization/store.css" rel="stylesheet" type="text/css">
		<link href="/styles/authorization/cart.css" rel="stylesheet" type="text/css">
		<link href="/styles/authorization/browse.css" rel="stylesheet" type="text/css">
		<link href="/styles/authorization/login.css" rel="stylesheet" type="text/css">
		<link href="/styles/authorization/shared_responsive.css" rel="stylesheet" type="text/css">

		<script type="text/javascript" src="/styles/authorization/jquery-1.8.3.min.js.download"></script>
		<script type="text/javascript">$J = jQuery.noConflict();</script>
		<script type="text/javascript" src="/styles/authorization/tooltip.js.download"></script>
		<script type="text/javascript" src="/styles/authorization/shared_global.js.download"></script>
		<script type="text/javascript" src="/styles/authorization/dynamicstore.js.download"></script>
		<script type="text/javascript" src="/styles/authorization/login.js.download"></script>
		<script type="text/javascript" src="/styles/authorization/shared_responsive_adapter.js.download"></script>
	</head>

	<body class="v6 login responsive_page">
		<div class="responsive_page_content">
			<div id="global_header">
				<div class="content">
					<div class="logo">
						<span id="logo_holder">
							<a href="">
								<img src="/styles/authorization/globalheader_logo.png" width="176" height="44">
							</a>
						</span>
					</div>
					<div class="supernav_container">
						<a class="menuitem supernav" href="" data-tooltip-type="selector" data-tooltip-content=".submenu_store">
    МАГАЗИН
						</a>
						<div class="submenu_store" style="display: none;" data-submenuid="store">
							<a class="submenuitem" href="">Популярное</a>
							<a class="submenuitem" href="">Список желаемого</a>
					       	<a class="submenuitem" href="">Новости</a>
							<a class="submenuitem" href="">Статистика</a>
						</div>
						<a class="menuitem supernav" style="display: block" href="" data-tooltip-type="selector" data-tooltip-content=".submenu_community">
    СООБЩЕСТВО
						</a>
						<div class="submenu_community" style="display: none;" data-submenuid="community">
							<a class="submenuitem" href="">Главная</a>
							<a class="submenuitem" href="">Обсуждения</a>
							<a class="submenuitem" href="">Мастерская</a>
							<a class="submenuitem" href="">Торговая площадка</a>
							<a class="submenuitem" href="">Трансляции</a>
						</div>
						<a class="menuitem" href="">
    О STEAM
</a>
						<a class="menuitem" href="">
    ПОДДЕРЖКА
						</a>
					</div>
					<script type="text/javascript">
    jQuery(function($) {
        $('#global_header .supernav').v_tooltip({'location':'bottom', 'destroyWhenDone': false, 'tooltipClass': 'supernav_content', 'offsetY':-4, 'offsetX': 1, 'horizontalSnap': 4, 'tooltipParent': '#global_header .supernav_container', 'correctForScreenSize': false});
						});
					</script>
				</div>
			</div>
			<div id="responsive_store_nav_ctn"></div><div data-cart-banner-spot="1"></div>
			<div class="responsive_page_template_content">
				<div id="store_header" class=""></div>
				<div class="page_content">
					<div id="error_display" class="checkout_error" style="display: none; color: #cc3300">
					</div>
					<div class="leftcol">
						<div class="checkout_content_box">
							<div class="loginbox">
								<div class="loginbox_left">
									<div class="loginbox_content">
										<h2>Войти</h2>
										<p>в существующий аккаунт Steam</p>
										<br>
										<form name="logon" action="" method="POST" id="login_form" style="display: block;">
											<div class="login_row">
												<div class="input_title">Имя аккаунта Steam</div>
												<input class="text_input" type="text" name="auth_username" id="input_username" value="">
											</div>
											<div class="login_row">
												<div class="input_title">Пароль</div>
												<input class="text_input" type="password" name="auth_password" id="input_password" autocomplete="off">
											</div>
											<div style="display: none;"><input type="submit"></div>
										</form>
										<noscript><p>Чтобы использовать этот сайт, необходимо включить JavaScript</p></noscript>
									</div>
									<div class="btn_ctn">
										<div id="login_btn_signin">
											<button type="submit" class="btnv6_blue_hoverfade  btn_medium">
												<span>Войти</span>
											</button>
										</div>
										<div id="login_btn_wait" style="display: none;">
											<img src="./Войти_files/throbber.gif">
										</div>
									</div>
								</div>
								<div class="loginbox_sep"></div>
								<div class="loginbox_right">
									<div class="loginbox_content">
										<h2>Создать</h2>
										<p>новый аккаунт</p>
										<br>
										<form name="logon" action="" method="POST" id="login_form" style="display: block;">
											<div class="login_row">
												<div class="input_title">Электронная почта</div>
												<input class="text_input" type="text" name="reg_email" id="input_username" value="">
											</div>
											<div class="login_row">
												<div class="input_title">Имя аккаунта Steam</div>
												<input class="text_input" type="password" name="reg_username" id="input_password" autocomplete="off">
											</div>
											<div class="login_row">
												<div class="input_title">Пароль</div>
												<input class="text_input" type="password" name="reg_password" id="input_password" autocomplete="off">
											</div>
											<div style="display: none;"><input type="submit"></div>
										</form>
									</div>
									<a target="_top" href="" class="btnv6_blue_hoverfade btn_medium">
										<span>Создать аккаунт</span>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div style="clear: both;"></div>
				</div>
			</div>
<!--			<div id="footer" class="">-->
<!--				<div class="footer_content">-->
<!--					<div class="rule"></div>-->
<!--	    			<div id="footer_text">-->
<!--    Copyright © 2020. PHP Merlion.-->
<!--	    			</div>-->
<!--	    			<div style="clear: left;"></div>-->
<!--					<br>-->
<!--					<div class="rule"></div>-->
<!--	            </div>-->
<!--			</div>-->
		</div>
	</body>
</html>