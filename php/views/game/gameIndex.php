<?php $reactParamStringName = 'fileNames';?>
<?php $gameObjectName = 'game';
$game = $$gameObjectName;?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Интернет магазин X</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/styles/carousel.css">
    <script type="application/javascript" src="https://unpkg.com/react@16.0.0/umd/react.production.min.js"></script>
    <script type="application/javascript" src="https://unpkg.com/react-dom@16.0.0/umd/react-dom.production.min.js"></script>
    <script type="application/javascript" src="https://unpkg.com/babel-standalone@6.26.0/babel.min.js"></script>
    <script type="text/babel" src="/carousel.js"></script>
    <script type="text/babel">
        ReactDOM.render(
            <Carousel names={<?=$$reactParamStringName?>} dirName="<?=$game['id']?>"/>,
            document.getElementById('carousel-holder')
        );
    </script>
</head>
<body>
<main>
    <div class="product">
        <div id="carousel-holder"></div>
        <div class="description">
            <div class="main-description">
                <h1><?=$game['name']?></h1>
                <div class="offer">
                    <h2 class="price"><?=$game['price']?> р.</h2>
                    <button class="buy">Купить</button>
                </div>
            </div>
            <p><?=$game['description']?></p>
        </div>
    </div>
</main>
</body>
</html>
