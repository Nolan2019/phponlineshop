<?php $gameArrayName = 'games';?>
<?php $paginationObjectName = 'pagination';?>
<?php $currentURI = rtrim($_SERVER['REQUEST_URI'], '/') . '/';
$currentURI = preg_replace('~/page[1-9][0-9]*~', '', $currentURI);?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Интернет магазин X</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/styles/adaptiveGrid.css">
</head>
<body>
<main>
    <ul class="products">
<?php $data = 'games';
foreach ($$gameArrayName as $game):?>
            <li class="product-wrapper">
                <a href="<?=$currentURI.$game['id']?>" class="product">
                    <div class="product-photo">
                        <img src="/images/games/posters/<?=$game['id']?>.webp" alt="<?=$game['name']?>">
                    </div>
                    <p><?=$game['name']?><br/><?=$game['developer']?><br/><?=$game['price']?> &#8381;</p>
                </a>
            </li>
<?php endforeach;?>
    </ul>
<?php echo $$paginationObjectName;?>
</main>
</body>