<?php


class Game
{
    private static array $fields = ['games.id AS id', 'developers.name AS developer', 'games.name AS name', 'price'];
    private static array $tables = ['games', 'developers'];

    public static function getGameById(int $id) {
        $db = Database::getConnection();
        $query = SqlQueryBuilder::getSql(
            ['games'],
            ['*'],
            null,
            ["id=$id"]);
        $result = $db->query($query);
        return $result->fetch();
    }

    public static function getGameCount(): int {
        $db = Database::getConnection();
        $query = SqlQueryBuilder::getSql(
            ['games'],
            ['COUNT(*)']);
        $result = $db->query($query);
        return $result->fetch()[0];
    }

    public static function getGames(int $limit=null, int $skip=null): array {
        $db = Database::getConnection();
        $query = SqlQueryBuilder::getSql(
            self::$tables,
            self::$fields,
            ['developer_id' => 'id'],
            null,
            null,
            true,
            $limit,
            $skip);
        $result = $db->query($query);

        $games = [];

        while ($row = $result->fetch()) {
            $games[] = $row;
        }

        return $games;
    }
}