<?php

return [
    'auth' => 'auth/index',
    'games/page([1-9][0-9]*)' => 'game/list/$1',
    'games/([1-9]+)' => 'game/index/$1',
    'games' => 'game/list',
];