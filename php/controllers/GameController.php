<?php

class GameController
{
    public function actionIndex(int $id) {
        $game = Game::getGameById($id);
        $fileNames = FileFinder::getFiles(ROOT.'/images/games/screenshots/' . $id);
        include_once(ROOT.'/php/views/game/gameIndex.php');
    }

    public function actionList(int $pageNumber=1) {
        $itemOnPageCount = 4;

        $skip = $itemOnPageCount * ($pageNumber - 1);

        $games = Game::getGames($itemOnPageCount, $skip);
        $itemCount = Game::getGameCount();

        $pagination = new Pagination($pageNumber, $itemCount, $itemOnPageCount, 'page');

        include_once(ROOT.'/php/views/game/gameGrid.php');
    }
}