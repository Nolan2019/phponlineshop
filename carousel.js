function LeftArrow(props) {
    return (
        <div onClick={props.onClick} className="left-arrow-container arrow-container">
            <img alt="" src="/images/left_arrow.png" className="left-arrow arrow"/>
        </div>
    );
}

function RightArrow(props) {
    return (
        <div onClick={props.onClick} className="right-arrow-container arrow-container">
            <img alt="" src="/images/right_arrow.png" className="right-arrow arrow"/>
        </div>
    );
}

class Carousel extends React.Component{
    constructor() {
        super();
        this.state = {
            imageNumber: 0
        };
        this.prevImage = this.prevImage.bind(this);
        this.nextImage = this.nextImage.bind(this);
    }

    prevImage() {
        if (this.state.imageNumber === 0)
            this.setState({
               imageNumber: this.props.names.length - 1
            });
        else
            this.setState({
                imageNumber: this.state.imageNumber - 1
            });
    }

    nextImage() {
        if (this.state.imageNumber === this.props.names.length - 1)
            this.setState({
                imageNumber: 0
            });
        else
            this.setState({
                imageNumber: this.state.imageNumber + 1
            });
    }

    render() {
        if (this.props.names.length > 0) {
            let imageName = this.props.names[this.state.imageNumber];
            let imagePath = "/images/games/screenshots/" + this.props.dirName + "/" + imageName;
            console.log(imagePath);
            return (
                <div className="product-photo">
                    <img src={imagePath} alt={imageName}/>
                    <LeftArrow onClick={this.prevImage}/>
                    <RightArrow onClick={this.nextImage}/>
                </div>
            );
        }

        return (
            <div className="space">
            </div>
        );
    }
}