<?php /** @noinspection PhpIncludeInspection */

// FRONT CONTROLLER

// настройки
ini_set('display_errors', 1);
error_reporting(E_ALL);

// констранты
define('ROOT', dirname(__FILE__));

// подключение компонентов
require_once ROOT.'/php/components/autoload.php';

$router = new Router();
$router->run();
